
#     Salarium- Backend : An Employee Payroll Application

## [Objective]()

```
   1. Design and implement the REST APIs to integrate with Sql database
      1. GET all Employees of a Company
      2. GET all Attendace, Leaves, Advance taken, Salary related data of specific Employee
      3. POST to add new Employee, Attendace, Leaves, Advance, Salary of Employees.
      4. PUT to update a existing Employees, Leaves, Attendace, Advance and Salaries.
      5. Delete a Employee, Leave, Attendace, Advance and Salary data.
  2. Implement APIs with the best REST Practices.
  3. Implement custom error handling in APIs to gracefully handle all exceptions.
  4. Use JWT Tokens and validate all endpoints for security.
  5. Implement Server-side Pagination and Sorting features. 
```

## API Reference

  * API Base url :- http://localhost:8080

  #### 1. Registration & Login To The Application

  Post Request - http://localhost:8080/api/v1/auth

  ```
  Expecting Json data-{username, password}
  ```

  Assumptions :- 

  ```
  1. username should be of Email Id format.
2. username and password can not be Null.

    Default Login Credentials:
                username: beleharshwardhan@gmail.com
                password: Admin@1234
  ```

  ![Registration Screenshot](https://gitlab.com/boot-projects/salarium/salarium-backend/-/blob/main/salarium/src/main/resources/Salarium_authenticate.png)
  ![Login Screenshot](/src/main/resources/Salarium_authenticate.png)


#### 2. GET all items
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
  GET /api/v1/items
```

| items | Description                       |
| :-------- | :-------------------------------- |
| `employees`      | fetch all the employees from database |
| `advances`      | fetch all the advance of all employees from database |
| `attendace`      | fetch all the attendace of  employees|
| `leaves`      | fetch all the leaves taken by employees|
| `salaries`      | fetch all the salary data of employees from database |


#### 3. GET all items with id
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
GET /api/v1/items/{id}
```

| `items/{id}` | Description                       |
| :-------- | :-------------------------------- |
| `employees/{id}`      | fetch employee from database with employeeID.  |
| `advances/{id}`      | fetch the advance from database with advanceID |
| `attendace/{id}`      | fetch the attendace of  employee with attendaceID|
| `leaves/{id}`      | fetch the leaves taken by employee with leaveID|
| `salaries/{id}`      | fetch the salary data of employee with salaryID |

#### 4. POST all items
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
POST /api/v1/items
```

| `items` | Description                       |
| :-------- | :-------------------------------- |
| `employees`      | Post one or more employees |
| `advances`      | Post one or more advance of all employees from database |
| `attendace`      | Post one or more attendace of  employees|
| `leaves`      | Post one or more leaves taken by employees|
| `salaries`      | Post one or more salary data of employees |


#### 5. Update all items with id
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
PUT /api/v1/items/{id}
```

| `items/{id}` | Description                       |
| :-------- | :-------------------------------- |
| `employees/{id}`      | Update employee from database with employeeID.  |
| `advances/{id}`      | Update the advance from database with advanceID |
| `attendace/{id}`      | Update the attendace of  employee with attendaceID|
| `leaves/{id}`      | Update the leaves taken by employee with leaveID|
| `salaries/{id}`      | Update the salary data of employee with salaryID |


#### 6. Delete items with id
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
DELETE /api/v1/items/{id}
```

| `items/{id}` | Description                       |
| :-------- | :-------------------------------- |
| `employees/{id}`      | Delete employee from database with employeeID.  |
| `advances/{id}`      | Delete the advance from database with advanceID |
| `attendace/{id}`      | Delete the attendace of  employee with attendaceID|
| `leaves/{id}`      | Delete the leaves taken by employee with leaveID|
| `salaries/{id}`      | Delete the salary data of employee with salaryID |


#### 7. GET items with Pagination
  ```
  expecting header - { 'Authorization', Bearer ${token} }.
```
```
GET /api/v1/items?offset='value'&pageSize='value'
```

| `items/{id}` | Description                       |
| :-------- | :-------------------------------- |
| `employees?offset='0'&pageSize='10'`      | Get the employees from database with pageSize 10 and offset 0.  |
| `advances?offset='2'&pageSize='20'`      | Get the advance from database with with pageSize 20 and offset 2 |
| `attendace?offset='0'&pageSize='5'`      | Get the attendace of  employee with with pageSize 5 and offset 0|
| `leaves?offset='0'&pageSize='2'`      | Get the leaves taken by employee with with pageSize 2 and offset 0|
| `salaries?offset='4'&pageSize='10'`      | Get the salary data of employee with with pageSize 10 and offset 4 |
