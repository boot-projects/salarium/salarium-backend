package com.salarium.dto;

public class EmployeeResponseDto {
	private int empId;
	
	private String empName;
	
	private String empContactNo;
	
	private String empAltContactNo;
	
	private String empEmail;
	
	private String addressLine1;
	
	private String addressLine2;
	
	private String pincode;
	
	private String city;
	
	private String state;
	
	private String bankName;
	
	private String ifsc;
	
	private String accountNo;
	
	private String bankBranch;
	
	private Double salary;
	
	private String employeeCode;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpContactNo() {
		return empContactNo;
	}

	public void setEmpContactNo(String empContactNo) {
		this.empContactNo = empContactNo;
	}

	public String getEmpAltContactNo() {
		return empAltContactNo;
	}

	public void setEmpAltContactNo(String empAltContactNo) {
		this.empAltContactNo = empAltContactNo;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public EmployeeResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeResponseDto(int empId, String empName, String empContactNo, String empAltContactNo, String empEmail,
			String addressLine1, String addressLine2, String pincode, String city, String state, String bankName,
			String ifsc, String accountNo, String bankBranch, Double salary, String employeeCode) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empContactNo = empContactNo;
		this.empAltContactNo = empAltContactNo;
		this.empEmail = empEmail;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.pincode = pincode;
		this.city = city;
		this.state = state;
		this.bankName = bankName;
		this.ifsc = ifsc;
		this.accountNo = accountNo;
		this.bankBranch = bankBranch;
		this.salary = salary;
		this.employeeCode = employeeCode;
	}

	@Override
	public String toString() {
		return "EmployeeResponseDto [empId=" + empId + ", empName=" + empName + ", empContactNo=" + empContactNo
				+ ", empAltContactNo=" + empAltContactNo + ", empEmail=" + empEmail + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", pincode=" + pincode + ", city=" + city + ", state=" + state
				+ ", bankName=" + bankName + ", ifsc=" + ifsc + ", accountNo=" + accountNo + ", bankBranch="
				+ bankBranch + ", salary=" + salary + ", employeeCode=" + employeeCode + "]";
	}
	
	
}
