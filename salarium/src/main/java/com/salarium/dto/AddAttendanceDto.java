package com.salarium.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class AddAttendanceDto {
	private int empId;
	
	private LocalDate attendanceDate;
	
	private LocalTime inTime;
	
	private LocalTime outTime;
	
	private boolean isFullDay;
	
	private boolean isHalfDay;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public LocalDate getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(LocalDate attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public LocalTime getInTime() {
		return inTime;
	}

	public void setInTime(LocalTime inTime) {
		this.inTime = inTime;
	}

	public LocalTime getOutTime() {
		return outTime;
	}

	public void setOutTime(LocalTime outTime) {
		this.outTime = outTime;
	}

	public boolean isFullDay() {
		return isFullDay;
	}

	public void setFullDay(boolean isFullDay) {
		this.isFullDay = isFullDay;
	}

	public boolean isHalfDay() {
		return isHalfDay;
	}

	public void setHalfDay(boolean isHalfDay) {
		this.isHalfDay = isHalfDay;
	}

	public AddAttendanceDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddAttendanceDto(int empId, LocalDate attendanceDate, LocalTime inTime, LocalTime outTime,
			boolean isFullDay, boolean isHalfDay) {
		super();
		this.empId = empId;
		this.attendanceDate = attendanceDate;
		this.inTime = inTime;
		this.outTime = outTime;
		this.isFullDay = isFullDay;
		this.isHalfDay = isHalfDay;
	}

	@Override
	public String toString() {
		return "AddAttendanceDto [empId=" + empId + ", attendanceDate=" + attendanceDate + ", inTime=" + inTime
				+ ", outTime=" + outTime + ", isFullDay=" + isFullDay + ", isHalfDay=" + isHalfDay + "]";
	}
	
	
}
