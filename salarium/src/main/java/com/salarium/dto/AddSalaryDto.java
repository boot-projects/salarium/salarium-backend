package com.salarium.dto;

import java.time.LocalDate;

public class AddSalaryDto {
	
	private LocalDate salaryDate;
	private Integer empId;
	private Double salaryAmount;
	
	public LocalDate getSalaryDate() {
		return salaryDate;
	}
	
	public void setSalaryDate(LocalDate salaryDate) {
		this.salaryDate = salaryDate;
	}
	
	public Integer getEmpId() {
		return empId;
	}
	
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public Double getSalaryAmount() {
		return salaryAmount;
	}
	
	public void setSalaryAmount(Double salaryAmount) {
		this.salaryAmount = salaryAmount;
	}

	public AddSalaryDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddSalaryDto(LocalDate salaryDate, Integer empId, Double salaryAmount) {
		super();
		this.salaryDate = salaryDate;
		this.empId = empId;
		this.salaryAmount = salaryAmount;
	}

	@Override
	public String toString() {
		return "AddSalaryDto [salaryDate=" + salaryDate + ", empId=" + empId + ", salaryAmount=" + salaryAmount + "]";
	}
	
	
	
}
