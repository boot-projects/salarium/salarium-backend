package com.salarium.dto;

import java.time.LocalDateTime;

public class AdvanceResponseDto {
	private int advanceId;
	
	private LocalDateTime advanceDate;
	
	private Double advanceAmount;
	
	private String reason;
	
	private int empId;


	public LocalDateTime getAdvanceDate() {
		return advanceDate;
	}

	public void setAdvanceDate(LocalDateTime advanceDate) {
		this.advanceDate = advanceDate;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getAdvanceId() {
		return advanceId;
	}

	public void setAdvanceId(int advanceId) {
		this.advanceId = advanceId;
	}

	public AdvanceResponseDto() {
		super();
	}

	public AdvanceResponseDto(int advanceId, LocalDateTime advanceDate, Double advanceAmount, String reason,
			int empId) {
		super();
		this.advanceId = advanceId;
		this.advanceDate = advanceDate;
		this.advanceAmount = advanceAmount;
		this.reason = reason;
		this.empId = empId;
	}

	@Override
	public String toString() {
		return "AdvanceResponseDto [advanceId=" + advanceId + ", advanceDate=" + advanceDate + ", advanceAmount="
				+ advanceAmount + ", reason=" + reason + ", empId=" + empId + "]";
	}
	
	
}
