package com.salarium.dto;

import java.time.LocalDateTime;

public class UpdateAdvanceDto {
	private int advanceId;
	
	private LocalDateTime advanceDate;
	
	private Double advanceAmount;
	
	private String reason;
	
	private int empId;


	public LocalDateTime getAdvanceDate() {
		return advanceDate;
	}

	public void setAdvanceDate(LocalDateTime advanceDate) {
		this.advanceDate = advanceDate;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getAdvanceId() {
		return advanceId;
	}

	public void setAdvanceId(int advanceId) {
		this.advanceId = advanceId;
	}

	public UpdateAdvanceDto(int advanceId, LocalDateTime advanceDate, Double advanceAmount, String reason, int empId) {
		super();
		this.advanceId = advanceId;
		this.advanceDate = advanceDate;
		this.advanceAmount = advanceAmount;
		this.reason = reason;
		this.empId = empId;
	}

	public UpdateAdvanceDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UpdateAdvanceDto [advanceId=" + advanceId + ", advanceDate=" + advanceDate + ", advanceAmount="
				+ advanceAmount + ", reason=" + reason + ", empId=" + empId + "]";
	}
	
	
	
	
}
