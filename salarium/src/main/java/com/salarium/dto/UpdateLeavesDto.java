package com.salarium.dto;

import java.time.LocalDate;

public class UpdateLeavesDto {
private int leaveId;
	
	private int empId;

	private LocalDate leaveDate;
	
	private String leaveReason;
	
	private Integer noOfFullDayLeaves;
	
	private Integer noOfHalfDayLeaves;

	public int getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public LocalDate getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(LocalDate leaveDate) {
		this.leaveDate = leaveDate;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public Integer getNoOfFullDayLeaves() {
		return noOfFullDayLeaves;
	}

	public void setNoOfFullDayLeaves(Integer noOfFullDayLeaves) {
		this.noOfFullDayLeaves = noOfFullDayLeaves;
	}

	public Integer getNoOfHalfDayLeaves() {
		return noOfHalfDayLeaves;
	}

	public void setNoOfHalfDayLeaves(Integer noOfHalfDayLeaves) {
		this.noOfHalfDayLeaves = noOfHalfDayLeaves;
	}

	public UpdateLeavesDto(int leaveId, int empId, LocalDate leaveDate, String leaveReason, Integer noOfFullDayLeaves,
			Integer noOfHalfDayLeaves) {
		super();
		this.leaveId = leaveId;
		this.empId = empId;
		this.leaveDate = leaveDate;
		this.leaveReason = leaveReason;
		this.noOfFullDayLeaves = noOfFullDayLeaves;
		this.noOfHalfDayLeaves = noOfHalfDayLeaves;
	}

	public UpdateLeavesDto() {
		super();
	}

	@Override
	public String toString() {
		return "UpdateLeavesDto [leaveId=" + leaveId + ", empId=" + empId + ", leaveDate=" + leaveDate
				+ ", leaveReason=" + leaveReason + ", noOfFullDayLeaves=" + noOfFullDayLeaves + ", noOfHalfDayLeaves="
				+ noOfHalfDayLeaves + "]";
	}
	
	
}
