package com.salarium.dto;

import java.time.LocalDate;

public class SalaryResponseDto {

	private Integer salaryId;
	private LocalDate salaryDate;
	private Integer empId;
	private Double presentDays;
	private Double salaryAmount;
	private Double totalAdvance;
	private Integer totalLeaves;
	
	public Integer getSalaryId() {
		return salaryId;
	}
	
	public void setSalaryId(Integer salaryId) {
		this.salaryId = salaryId;
	}
	
	public LocalDate getSalaryDate() {
		return salaryDate;
	}
	
	public void setSalaryDate(LocalDate salaryDate) {
		this.salaryDate = salaryDate;
	}
	
	public Integer getEmpId() {
		return empId;
	}
	
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public Double getPresentDays() {
		return presentDays;
	}
	
	public void setPresentDays(Double presentDays) {
		this.presentDays = presentDays;
	}
	
	public Double getSalaryAmount() {
		return salaryAmount;
	}
	
	public void setSalaryAmount(Double salaryAmount) {
		this.salaryAmount = salaryAmount;
	}
	
	public Double getTotalAdvance() {
		return totalAdvance;
	}
	
	public void setTotalAdvance(Double totalAdvance) {
		this.totalAdvance = totalAdvance;
	}
	
	public Integer getTotalLeaves() {
		return totalLeaves;
	}
	
	public void setTotalLeaves(Integer totalLeaves) {
		this.totalLeaves = totalLeaves;
	}
	
	public SalaryResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SalaryResponseDto(Integer salaryId, LocalDate salaryDate, Integer empId, Double presentDays,
			Double salaryAmount, Double totalAdvance, Integer totalLeaves) {
		super();
		this.salaryId = salaryId;
		this.salaryDate = salaryDate;
		this.empId = empId;
		this.presentDays = presentDays;
		this.salaryAmount = salaryAmount;
		this.totalAdvance = totalAdvance;
		this.totalLeaves = totalLeaves;
	}
	
	@Override
	public String toString() {
		return "SalaryResponseDto [salaryId=" + salaryId + ", salaryDate=" + salaryDate + ", empId=" + empId
				+ ", presentDays=" + presentDays + ", salaryAmount=" + salaryAmount + ", totalAdvance=" + totalAdvance
				+ ", totalLeaves=" + totalLeaves + "]";
	}
	
}
