package com.salarium.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.salarium.entities.Salary;

import jakarta.transaction.Transactional;

public interface SalaryRepo extends JpaRepository<Salary, Integer>, JpaSpecificationExecutor<Salary> {

	Optional<Salary> findBySalaryId(int id);
	
	@Query("Select a.salaryEmployee,a.salaryId,a.totalAdvance, a.salaryDate, a.salaryAmount, a.presentDays FROM Salary a where a.salaryEmployee= :id AND a.salaryDate BETWEEN :startDate AND :endDate")
	List<Salary> findSalaryByEmpId(Integer id, LocalDateTime startDate, LocalDateTime endDate);
	
	@Transactional
	@Modifying
	int deleteBySalaryId(Integer id);

}
