package com.salarium.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Query;

import com.salarium.entities.Leaves;

import jakarta.transaction.Transactional;

public interface LeavesRepo extends JpaRepository<Leaves, Integer>, JpaSpecificationExecutor<Leaves>{
	@Modifying
	@Transactional
	int deleteByleaveId(Integer id);
	
	
//	@Modifying
//	@Transactional
////	@Query("PDATE Leaves l S")
//	int updateLeaves(Leaves leave);
	@Transactional
	List<Leaves> findAll();


	Leaves findByLeaveId(int leaveId);

	@Modifying
	@Transactional
	@Query("Select l.leaveId, l.leaveEmployee, l.leaveDate, l.leaveReason, l.noOfFullDayLeaves, l.noOfHalfDayLeaves from Leaves l WHERE l.leaveEmployee = :empId AND l.leaveDate BETWEEN :startDate AND :endDate")
	List<Leaves> findLeavesByEmpId(Integer empId, LocalDateTime startDate, LocalDateTime endDate);

}
