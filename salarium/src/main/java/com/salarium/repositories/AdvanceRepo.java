package com.salarium.repositories;



import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.salarium.entities.Advance;

import jakarta.transaction.Transactional;

public interface AdvanceRepo extends JpaRepository<Advance, Integer>, JpaSpecificationExecutor<Advance> {

	Optional<Advance> findByAdvanceId(Integer advanceId);
	
	@Query("Select a.advanceEmployee,a.advanceId, a.advanceDate, a.advanceAmount, a.reason FROM Advance a where a.advanceEmployee= :id")
	List<Advance> findAdvanceByEmpId(Integer id);

	@Transactional
	int deleteByAdvanceId(Integer id);
	
	

}
