package com.salarium.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.salarium.entities.Attendance;

import jakarta.transaction.Transactional;

public interface AttendanceRepo extends JpaRepository<Attendance, Integer>, JpaSpecificationExecutor<Attendance> {

	Optional<Attendance> findByAttendanceId(Integer id);
	
	@Query("SELECT a.attendanceId, a.attendanceEmployee, a.attendanceDate, a.inTime, a.outTime, a.isFullDay, a.isHalfDay from Attendance a WHERE a.attendanceEmployee =:id AND a.attendanceDate BETWEEN :startDate AND :endDate")
	List<Attendance> findAttendanceByEmpId(Integer id, LocalDateTime startDate, LocalDateTime endDate);
	
	@Transactional
	int deleteByAttendanceId(Integer id);

}
