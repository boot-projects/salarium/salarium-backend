package com.salarium.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;

import com.salarium.entities.Employee;

import jakarta.transaction.Transactional;

public interface EmployeeRepo extends JpaRepository<Employee, Integer>, JpaSpecificationExecutor<Employee> {
	
	@Modifying
	@Transactional
	int deleteByEmpId(Integer empId);

	Optional<Employee> findByEmpId(Integer id);
	
	@Modifying
	@Transactional
	void deleteAllByEmpId(Integer id);
 
	
	
}
