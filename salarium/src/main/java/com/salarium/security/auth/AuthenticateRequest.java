package com.salarium.security.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticateRequest {
	private String email;
	private String password;
	
	public AuthenticateRequest(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	public AuthenticateRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "AuthenticateRequest [email=" + email + ", password=" + password + "]";
	}
	
	
}
