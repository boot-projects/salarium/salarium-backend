package com.salarium.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.salarium.dto.AddAttendanceDto;
import com.salarium.dto.AttendanceResponseDto;
import com.salarium.dto.UpdateAttendanceDto;
import com.salarium.services.AttendanceService;

@RestController
@RequestMapping("/api/v1/attendance")
public class AttendanceController {
	
	private final AttendanceService attendanceService;
	
	public AttendanceController(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}
	
	@GetMapping
	public ResponseEntity<List<AttendanceResponseDto>> getAttendance(){
		List<AttendanceResponseDto> dtoList = this.attendanceService.getAllAttendance();
		return new ResponseEntity<>(dtoList, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<AttendanceResponseDto> getAttendaceeById(@PathVariable("id") Integer id){
		AttendanceResponseDto attendanceResponseDto = this.attendanceService.getAttendanceById(id);
		return new ResponseEntity<>(attendanceResponseDto, HttpStatus.OK);
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<List<AttendanceResponseDto>> getAttendanceByEmpId(@PathVariable Integer id, @RequestParam("startDate") LocalDateTime startDate,@RequestParam("endDate") LocalDateTime endDate){
		List<AttendanceResponseDto> dtoList = this.attendanceService.getAttendaceByEmpId(id,startDate,endDate);
		return new ResponseEntity<>(dtoList,HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<AttendanceResponseDto> addAttendance(@RequestBody AddAttendanceDto addAttendanceDto){   
		AttendanceResponseDto attendanceResponseDto = this.attendanceService.addAttendance(addAttendanceDto);
		return new ResponseEntity<>(attendanceResponseDto, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AttendanceResponseDto> updateAttendance(@PathVariable("id") Integer id,@RequestBody UpdateAttendanceDto updateAttendanceDto){   //on pers of convention it is right
		AttendanceResponseDto attendanceResponseDto = this.attendanceService.updateAttendance(id,updateAttendanceDto);
		return new ResponseEntity<>(attendanceResponseDto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAttendanceById(@PathVariable("id") Integer id){
		int count = this.attendanceService.deleteAttendance(id);
		if(count>0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
