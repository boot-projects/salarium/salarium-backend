package com.salarium.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.salarium.dto.AddSalaryDto;
import com.salarium.dto.SalaryResponseDto;
import com.salarium.dto.UpdateSalaryDto;
import com.salarium.services.SalaryService;

@RestController
@RequestMapping("/api/v1/salaries")
public class SalaryController {
	private final SalaryService salaryService;
	
	public SalaryController(SalaryService salaryService) {
		this.salaryService = salaryService;
	}
	
	@PostMapping("")
	public ResponseEntity<SalaryResponseDto> addSalary(@RequestBody AddSalaryDto addSalaryDto){
		SalaryResponseDto salaryResponseDto =  this.salaryService.addSalary(addSalaryDto);
		return new ResponseEntity<>(salaryResponseDto,HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<SalaryResponseDto> updateSalary(@PathVariable("id") Integer salaryId, @RequestBody UpdateSalaryDto updateSalaryDto){
		SalaryResponseDto salaryResponseDto = this.salaryService.updateSalary(salaryId, updateSalaryDto);
		return new ResponseEntity<>(salaryResponseDto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteSalary(@PathVariable("id") Integer salaryId){
		int rowsAffected =  this.salaryService.deleteSalary(salaryId);
		if(rowsAffected >0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping()
	public ResponseEntity<List<SalaryResponseDto>> getAllSalaries(){
		List<SalaryResponseDto> salaryResponseDtoList = this.salaryService.getAllSalaries();
		return new ResponseEntity<>(salaryResponseDtoList,HttpStatus.OK);
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<List<SalaryResponseDto>> getSalaryByEmpId(@PathVariable("id") Integer empId, LocalDateTime startDate, LocalDateTime endDate){
		List<SalaryResponseDto> salaryResponseDtoList = this.salaryService.getSalaryByEmpId(empId, startDate, endDate);
		return new ResponseEntity<>(salaryResponseDtoList, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SalaryResponseDto> getSalaryById(@PathVariable("id") Integer salaryId){
		SalaryResponseDto salaryResponseDto = this.salaryService.getSalaryById(salaryId);
		return new ResponseEntity<>(salaryResponseDto, HttpStatus.OK);
	}
}
