 package com.salarium.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.salarium.dto.AddAdvanceDto;
import com.salarium.dto.AdvanceResponseDto;
import com.salarium.dto.UpdateAdvanceDto;
import com.salarium.services.AdvanceService;

@RestController
@RequestMapping("/api/v1/advances")
public class AdvanceController {
	
	private final AdvanceService advanceService;
	
	
	public AdvanceController(AdvanceService advanceService) {
		this.advanceService = advanceService;
	}
	
	@PostMapping("")
	public ResponseEntity<AdvanceResponseDto> addAdvance(@RequestBody AddAdvanceDto addAdvanceDto){
		AdvanceResponseDto advanceResponseDto =  this.advanceService.addAdvance(addAdvanceDto);
		return new ResponseEntity<>(advanceResponseDto,HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AdvanceResponseDto> updateAdvance(@PathVariable("id") Integer advanceId, @RequestBody UpdateAdvanceDto updateAdvanceDto){
		AdvanceResponseDto advanceResponseDto = this.advanceService.updateAdvance(advanceId, updateAdvanceDto);
		return new ResponseEntity<>(advanceResponseDto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAdvance(@PathVariable("id") Integer advanceId){
		int rowsAffected =  this.advanceService.deleteAdvance(advanceId);
		if(rowsAffected >0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping()
	public ResponseEntity<List<AdvanceResponseDto>> getAllAdvances(){
		List<AdvanceResponseDto> advanceResponseDtoList = this.advanceService.getAllAdvance();
		return new ResponseEntity<>(advanceResponseDtoList,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<AdvanceResponseDto> getAdvanceByAdvanceId(@PathVariable("id") Integer id){
		AdvanceResponseDto advanceResponseDto = this.advanceService.getAdvanceById(id);
		return new ResponseEntity<>(advanceResponseDto,HttpStatus.OK);
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<List<AdvanceResponseDto>> getAdvanceByEmpId(@PathVariable("id") Integer empId){
		List<AdvanceResponseDto> advanceResponseDtoList = this.advanceService.getAdvanceByEmpId(empId);
		return new ResponseEntity<>(advanceResponseDtoList, HttpStatus.OK);
	}
}
