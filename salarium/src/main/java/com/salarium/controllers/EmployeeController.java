package com.salarium.controllers;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.salarium.dto.AddEmployeeDto;
import com.salarium.dto.EmployeeResponseDto;
import com.salarium.dto.UpdateEmployeeDto;
import com.salarium.services.EmployeeService;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {
	
	private final EmployeeService employeeService;
	
	public EmployeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	@GetMapping("")
	public ResponseEntity<List<EmployeeResponseDto>> getEmployees(){
		List<EmployeeResponseDto> allEmployees = this.employeeService.getAllEmployees();
		return new ResponseEntity<>(allEmployees,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<EmployeeResponseDto> getEmployeeById(@PathVariable("id") Integer id){
		EmployeeResponseDto employee = this.employeeService.getEmployeeById(id);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	

	@PostMapping("")
	public ResponseEntity<EmployeeResponseDto> addEmployee(@RequestBody AddEmployeeDto addEmployeeDto){
		var emp = this.employeeService.addEmployee(addEmployeeDto);
		if(emp instanceof EmployeeResponseDto) {
//			return new ResponseEntity<>(emp, HttpStatus.CREATED);
			return ResponseEntity.ok(emp);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	@PutMapping("/{id}")
	public ResponseEntity<EmployeeResponseDto> updateEmployee(@PathVariable("id") Integer id, @RequestBody UpdateEmployeeDto updateEmployeeDto){
		var emp = this.employeeService.updateEmployee(id, updateEmployeeDto);
		
		if(emp != null) {
			return new ResponseEntity<>(emp,HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable("id") int id){
		int count = this.employeeService.deleteEmployee(id);
		if(count > 0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/pages")
	public ResponseEntity<Page<EmployeeResponseDto>> getEmployeesWithPagination(@RequestParam(name = "offset") int offset, @RequestParam(name = "pageSize") int pageSize){
		Page<EmployeeResponseDto> pages = this.employeeService.getEmployeeswithPagination(offset, pageSize);
		return new ResponseEntity<>(pages, HttpStatus.OK);
	}
	
}
