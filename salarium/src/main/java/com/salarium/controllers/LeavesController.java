package com.salarium.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.salarium.dto.AddLeavesDTO;
import com.salarium.dto.LeavesResponseDto;
import com.salarium.dto.UpdateLeavesDto;
import com.salarium.services.LeavesService;

@RestController
@RequestMapping("/api/v1/leaves")
public class LeavesController {
	
	private final LeavesService leavesService;
	
	public LeavesController(LeavesService leavesService) {
		this.leavesService = leavesService;
	}
	
	@PostMapping("")
	public ResponseEntity<LeavesResponseDto> addLeave(@RequestBody AddLeavesDTO leaveDto){
		var leavesResponseDto =  this.leavesService.addLeave(leaveDto);
		return new ResponseEntity<>(leavesResponseDto, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateLeave(@PathVariable("id") Integer id,@RequestBody UpdateLeavesDto updateLeavesDto){
		var leavesResponseDto = this.leavesService.updateLeaves(id, updateLeavesDto);
		if(leavesResponseDto!= null) {
			return new ResponseEntity<>(leavesResponseDto, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable("{id}") Integer id){
		int rowsAffected =  this.leavesService.deleteLeave(id);
		if(rowsAffected >0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("")
	public ResponseEntity<List<LeavesResponseDto>> getAllLeaves(){
		List<LeavesResponseDto> list = this.leavesService.getAllLeaves();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/{leaveId}")
	public ResponseEntity<LeavesResponseDto> getLeaveById(@PathVariable("leaveId")Integer leaveId){
		LeavesResponseDto leavesResponseDto = this.leavesService.getByLeaveId(leaveId);
		
		return new ResponseEntity<>(leavesResponseDto, HttpStatus.OK);
	}
	
	@GetMapping("/employees/{empId}")
	public ResponseEntity<List<LeavesResponseDto>> getLeavesByEmpId(@PathVariable("empId")Integer empId, @RequestParam("startDate") LocalDateTime startDate, @RequestParam("endDate") LocalDateTime endDate){
		List<LeavesResponseDto> list = this.leavesService.getAllByEmpId(empId, startDate, endDate);
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
}
