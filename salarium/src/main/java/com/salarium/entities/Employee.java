package com.salarium.entities;

import java.io.Serializable;
import java.util.List;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
//import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Employee implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer empId;
	
	@Column(nullable = false)
	private String empName;
	
//	private String gender;
//	private String dob;
	@Column(nullable = false)
	private String empContactNo;
	@Column(nullable = true)
	private String empAltContactNo;
	
	@Column(nullable = false)
	private String empEmail;
	
	@Column(nullable = false)
	private String addressLine1;
	@Column(nullable = true)
	private String addressLine2;
	
	@Column(nullable = false)
	private String pincode;
	@Column(nullable = false)
	private String city;
	@Column(nullable = false)
	private String state;
	
	@Column(nullable = false)
	private String bankName;
	@Column(nullable = false)
	private String ifsc;
	@Column(nullable = false)
	private String accountNo;
	
	private String bankBranch;
	
	@Column(nullable = false)
	private Double salary;
	
	@Column(nullable = false, updatable = false)
	private String employeeCode;
	
	
	@OneToMany(mappedBy = "leaveEmployee", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Leaves.class)
	private List<Leaves> leaves;

	@OneToMany(mappedBy = "advanceEmployee",cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Advance.class)
	private List<Advance> advances;
	
	@OneToMany(mappedBy = "attendanceEmployee",cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Attendance.class)
	private List<Attendance> attendance;
	
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpContactNo() {
		return empContactNo;
	}

	public void setEmpContactNo(String empContactNo) {
		this.empContactNo = empContactNo;
	}

	public String getEmpAltContactNo() {
		return empAltContactNo;
	}

	public void setEmpAltContactNo(String empAltContactNo) {
		this.empAltContactNo = empAltContactNo;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public List<Leaves> getLeaves() {
		return leaves;
	}

	public void setLeaves(List<Leaves> leaves) {
		this.leaves = leaves;
	}

	public List<Advance> getAdvances() {
		return advances;
	}

	public void setAdvances(List<Advance> advances) {
		this.advances = advances;
	}
	
	public List<Attendance> getAttendance() {
		return attendance;
	}

	public void setAttendance(List<Attendance> attendance) {
		this.attendance = attendance;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Employee(Integer empId, String empName, String empContactNo, String empAltContactNo, String empEmail,
			String addressLine1, String addressLine2, String pincode, String city, String state, String bankName,
			String ifsc, String accountNo, String bankBranch, Double salary, List<Leaves> leaves,
			List<Advance> advances, String employeeCode) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empContactNo = empContactNo;
		this.empAltContactNo = empAltContactNo;
		this.empEmail = empEmail;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.pincode = pincode;
		this.city = city;
		this.state = state;
		this.bankName = bankName;
		this.ifsc = ifsc;
		this.accountNo = accountNo;
		this.bankBranch = bankBranch;
		this.salary = salary;
		this.leaves = leaves;
		this.advances = advances;
		this.employeeCode = employeeCode;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empContactNo=" + empContactNo
				+ ", empAltContactNo=" + empAltContactNo + ", empEmail=" + empEmail + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", pincode=" + pincode + ", city=" + city + ", state=" + state
				+ ", bankName=" + bankName + ", ifsc=" + ifsc + ", accountNo=" + accountNo + ", bankBranch="
				+ bankBranch + ", salary=" + salary + ", employeeCode=" + employeeCode + ", leaves=" + leaves
				+ ", advances=" + advances + ", attendance=" + attendance + "]";
	}

	
}
