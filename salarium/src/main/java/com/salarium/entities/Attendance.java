package com.salarium.entities;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Attendance implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "attendanceId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer attendanceId;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "employee")
	private Employee attendanceEmployee;
	
	
	private LocalDate attendanceDate;
	
	private LocalTime inTime;
	
	private LocalTime outTime;
	
	private boolean isFullDay;
	
	private boolean isHalfDay;

	public Integer getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Integer attendanceId) {
		this.attendanceId = attendanceId;
	}

	public Employee getEmployee() {
		return attendanceEmployee;
	}

	public void setEmployee(Employee attendanceEmployee) {
		this.attendanceEmployee = attendanceEmployee;
	}

	public LocalDate getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(LocalDate attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public LocalTime getInTime() {
		return inTime;
	}

	public void setInTime(LocalTime inTime) {
		this.inTime = inTime;
	}

	public LocalTime getOutTime() {
		return outTime;
	}

	public void setOutTime(LocalTime outTime) {
		this.outTime = outTime;
	}
	
	public void setFullDay(LocalTime inTime, LocalTime outTime) {
		this.isFullDay = checkFullDay(inTime, outTime);
	}

	public boolean getIsFullDay() {
		return isFullDay;
	}
	
	public boolean getIsHalfDay() {
		return isHalfDay;
	}

	public void setHalfDay(LocalTime inTime, LocalTime outTime) {
		this.isHalfDay = isFullDay ? false : checkIsHalfDay(inTime, outTime);
	}

	public Attendance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Attendance(Integer attendanceId, Employee attendanceEmployee, LocalDate attendanceDate, LocalTime inTime,
			LocalTime outTime) {
		super();
		this.attendanceId = attendanceId;
		this.attendanceEmployee = attendanceEmployee;
		this.attendanceDate = attendanceDate;
		this.inTime = inTime;
		this.outTime = outTime;
		this.isFullDay = checkFullDay(inTime, outTime);
		this.isHalfDay = isFullDay ? false : checkIsHalfDay(inTime, outTime);
	}

	private boolean checkFullDay(LocalTime inTime, LocalTime outTime) {
		Duration duration = Duration.between(inTime, outTime);
		
		return duration.toHours() >= 8;
	}

	private boolean checkIsHalfDay(LocalTime inTime, LocalTime outTime) {
		Duration duration = Duration.between(inTime, outTime);
		
		return duration.toHours() >= 4;
	}

	@Override
	public String toString() {
		return "Attendance [id=" + attendanceId + ", attendanceEmployee=" + attendanceEmployee + ", attendanceDate=" + attendanceDate + ", inTime="
				+ inTime + ", outTime=" + outTime + ", isFullDay=" + isFullDay + "]";
	}
	
	
}
