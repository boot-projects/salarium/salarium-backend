package com.salarium.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Value;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Salary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer salaryId;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	private Employee salaryEmployee;
	
	
	private LocalDate salaryDate;
	
//	@Value("${this.salaryEmployee.getAdvances().stream().mapToDouble(a -> a.getAdvanceAmount()).sum()}")
	private Double totalAdvance;
	
//	@Value("${this.salaryEmployee.getAttendance().stream().mapToDouble(a -> a.isFullDay ? 1 : a.isHalfDay ? (1/2) : 0).sum()}")
	private Double presentDays;
	
//	@Value("${this.salaryEmployee.getleaves().stream().mapToDouble(a -> (a.noOfFullDayLeaves+((1/2)* a.noOfHalfDayLeaves))).sum()}")
	private Double totalLeaves; 
	
	private Double salaryAmount;

	public Integer getId() {
		return salaryId;
	}

	public void setId(Integer salaryId) {
		this.salaryId = salaryId;
	}

	public Employee getSalaryEmployee() {
		return salaryEmployee;
	}

	public void setSalaryEmployee(Employee salaryEmployee) {
		this.salaryEmployee = salaryEmployee;
	}

	public LocalDate getSalaryDate() {
		return this.salaryDate;
	}

	public void setSalaryDate(LocalDate salaryDate) {
		this.salaryDate = salaryDate;
	}

	public Double gettotalAdvance() {
		return totalAdvance; 
	}

	public void settotalAdvance(Double totalAdvance) {
		this.totalAdvance = this.salaryEmployee.getAdvances().stream().mapToDouble(a -> a.getAdvanceAmount()).sum();
	}

	public Double getpresentDays() {
		return presentDays;
	}

	public void setpresentDays(Double presentDays) {
		this.presentDays = this.salaryEmployee.getAttendance().stream().mapToDouble(a -> a.getIsFullDay() ? 1 : a.getIsHalfDay() ? (1/2) : 0).sum();
	}

	public Double getsalaryAmount() {
		return salaryAmount;
	}

	public void setsalaryAmount(Double salaryAmount) {
		this.salaryAmount = this.salaryEmployee.getSalary();
	}

	public Double getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(Double totalLeaves) {
		this.totalLeaves = this.salaryEmployee.getLeaves().stream().mapToDouble(a -> (a.getNoOfFullDayLeaves()+((1/2)* a.getNoOfHalfDayLeaves()))).sum();
	}

	public Salary() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Salary(Integer salaryId, Employee salaryEmployee, LocalDate salaryDate, Double totalAdvance, Double presentDays,
			Double salaryAmount, Double totalLeaves) {
		super();
		this.salaryId = salaryId;
		this.salaryEmployee = salaryEmployee;
		this.salaryDate = salaryDate;
		this.totalAdvance = totalAdvance;
		this.presentDays = presentDays;
		this.salaryAmount = salaryAmount;
		this.totalLeaves = totalLeaves;
	}

	@Override
	public String toString() {
		return "Salary [id=" + salaryId + ", salaryEmployee=" + salaryEmployee + ", SalaryDate=" + salaryDate + ", totalAdvance="
				+ totalAdvance + ", presentDays=" + presentDays + ", totalLeaves=" + totalLeaves + ", salaryAmount=" + salaryAmount + "]";
	}
	
//	private Double gettotalAdvances() {
//		return this.salaryEmployee.advances.stream().mapToDouble(a -> a.advanceAmount).sum();
//	}
	
	
	
}
