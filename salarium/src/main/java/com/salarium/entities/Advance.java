package com.salarium.entities;

import java.time.LocalDateTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Advance {

	
	@Id
	@GeneratedValue
	private Integer advanceId;
	
	@Column(nullable = false)
	private LocalDateTime advanceDate;
	
	@Column(nullable = false)
	private Double advanceAmount;
	
	private String reason;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeId")
	private Employee advanceEmployee;
	
	public Integer getAdvanceId() {
		return advanceId;
	}

	public void setAdvanceId(Integer advanceId) {
		this.advanceId = advanceId;
	}

	public LocalDateTime getAdvanceDate() {
		return advanceDate;
	}

	public void setAdvanceDate(LocalDateTime advanceDate) {
		this.advanceDate = advanceDate;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Employee getEmployee() {
		return advanceEmployee;
	}

	public void setEmployee(Employee advanceEmployee) {
		this.advanceEmployee = advanceEmployee;
	}

	public Advance(Integer advanceId, LocalDateTime advanceDate, Double advanceAmount, String reason,
			Employee advanceEmployee) {
		super();
		this.advanceId = advanceId;
		this.advanceDate = advanceDate;
		this.advanceAmount = advanceAmount;
		this.reason = reason;
		this.advanceEmployee = advanceEmployee;
	}

	public Advance() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Advance [advanceId=" + advanceId + ", advanceDate=" + advanceDate + ", advanceAmount=" + advanceAmount
				+ ", reason=" + reason + ", advanceEmployee=" + advanceEmployee + "]";
	}
	
	
}
