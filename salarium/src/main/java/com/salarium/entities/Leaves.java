package com.salarium.entities;



import java.io.Serializable;
import java.time.LocalDate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Builder;

@Entity
@Builder
public class Leaves implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int leaveId;
	
	@ManyToOne(cascade = CascadeType.ALL,  fetch = FetchType.LAZY)
	@JoinColumn(name = "employee")
	private Employee leaveEmployee;
	
	@Column(nullable = false)
	private LocalDate leaveDate;
	@Column(nullable = false)
	private String leaveReason;
	@Column(nullable = false)
	private int noOfFullDayLeaves;
	@Column(nullable = false)
	private int noOfHalfDayLeaves;
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public Employee getEmployee() {
		return leaveEmployee;
	}
	public void setEmployee(Employee leaveEmployee) {
		this .leaveEmployee = leaveEmployee;
	}
	public LocalDate getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(LocalDate leaveDate) {
		this.leaveDate = leaveDate;
	}
	public String getLeaveReason() {
		return leaveReason;
	}
	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}
	public int getNoOfFullDayLeaves() {
		return noOfFullDayLeaves;
	}
	public void setNoOfFullDayLeaves(int noOfFullDayLeaves) {
		this.noOfFullDayLeaves = noOfFullDayLeaves;
	}
	public int getNoOfHalfDayLeaves() {
		return noOfHalfDayLeaves;
	}
	public void setNoOfHalfDayLeaves(int noOfHalfDayLeaves) {
		this.noOfHalfDayLeaves = noOfHalfDayLeaves;
	}
	public Leaves(int leaveId, Employee leaveEmployee, LocalDate leaveDate, String leaveReason, int noOfFullDayLeaves,
			int noOfHalfDayLeaves) {
		super();
		this.leaveId = leaveId;
		this.leaveEmployee = leaveEmployee;
		this.leaveDate = leaveDate;
		this.leaveReason = leaveReason;
		this.noOfFullDayLeaves = noOfFullDayLeaves;
		this.noOfHalfDayLeaves = noOfHalfDayLeaves;
	}
	public Leaves() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
