package com.salarium.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddAdvanceDto;
import com.salarium.dto.AdvanceResponseDto;
import com.salarium.dto.UpdateAdvanceDto;
import com.salarium.entities.Advance;
import com.salarium.mapper.AdvanceMapper;
import com.salarium.repositories.AdvanceRepo;


@Service
public class AdvanceService {
	private final AdvanceRepo advanceRepo;
	private final AdvanceMapper advanceMapper;
	
	public AdvanceService(AdvanceRepo advanceRepo, AdvanceMapper advanceMapper) {
		this.advanceRepo = advanceRepo;
		this.advanceMapper = advanceMapper;
	}
	
	public AdvanceResponseDto addAdvance(AddAdvanceDto addAdvanceDto){
		Advance newAdvance = this.advanceMapper.toAdvance(addAdvanceDto);
		newAdvance = this.advanceRepo.save(newAdvance);
		
		return this.advanceMapper.toAdvanceResponseDto(newAdvance);
		
	}
	
	public AdvanceResponseDto updateAdvance(Integer advanceId, UpdateAdvanceDto updateAdvanceDto){
		var existingAdvance = this.advanceRepo.findByAdvanceId(advanceId);

		if (existingAdvance != null) {
			var updatedAdvance = this.advanceMapper.toAdvance(advanceId, updateAdvanceDto);
			updatedAdvance = this.advanceRepo.save(updatedAdvance);

			return this.advanceMapper.toAdvanceResponseDto(updatedAdvance);
		}
		return null;
	}

	public int deleteAdvance(Integer id){
		return this.advanceRepo.deleteByAdvanceId(id);
	}
	
	public List<AdvanceResponseDto> getAllAdvance(){
		List<Advance> list = this.advanceRepo.findAll();
		List<AdvanceResponseDto> advanceResponseDtoList = new ArrayList<>();
		
		for(Advance i : list) {
			advanceResponseDtoList.add(this.advanceMapper.toAdvanceResponseDto(i));
		}
		return advanceResponseDtoList;
	}
	
	public AdvanceResponseDto getAdvanceById(Integer advanceId){
		Optional<Advance> advOptional = this.advanceRepo.findByAdvanceId(advanceId);
		Advance advance = advOptional.get();
		return this.advanceMapper.toAdvanceResponseDto(advance);
	}
	
	public List<AdvanceResponseDto> getAdvanceByEmpId(Integer empId){
		List<Advance> advanceList = this.advanceRepo.findAdvanceByEmpId(empId);
		List<AdvanceResponseDto> advanceResponseDtoList = new ArrayList<>();
		
		for(Advance i : advanceList) {
			advanceResponseDtoList.add(this.advanceMapper.toAdvanceResponseDto(i));
		}
		
		return advanceResponseDtoList;
	}
	
}
