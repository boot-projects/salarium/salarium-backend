package com.salarium.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddAttendanceDto;
import com.salarium.dto.AttendanceResponseDto;
import com.salarium.dto.UpdateAttendanceDto;
import com.salarium.entities.Attendance;
import com.salarium.mapper.AttendanceMapper;
import com.salarium.repositories.AttendanceRepo;

@Service
public class AttendanceService {
	private final AttendanceRepo attendanceRepo;
	private final AttendanceMapper attendanceMapper;
	
	public AttendanceService(AttendanceRepo attendanceRepo, AttendanceMapper attendanceMapper) {
		this.attendanceRepo = attendanceRepo;
		this.attendanceMapper = attendanceMapper;
	}
	
	public AttendanceResponseDto addAttendance(AddAttendanceDto attendanceDto){
		Attendance attendance = this.attendanceMapper.toAttendance(attendanceDto);
		attendance = this.attendanceRepo.save(attendance);
		return this.attendanceMapper.toAttendanceResponseDto(attendance);
	}
	
	public AttendanceResponseDto updateAttendance(Integer existingAttId, UpdateAttendanceDto updateAttendanceDto){    //just change the query to update
		var existingAttendance = this.attendanceRepo.findByAttendanceId(existingAttId);

		if (existingAttendance != null) {
			Attendance updatedAttendance = this.attendanceMapper.toAttendance(existingAttId, updateAttendanceDto);
			updatedAttendance = this.attendanceRepo.save(updatedAttendance);

			return this.attendanceMapper.toAttendanceResponseDto(updatedAttendance);
		}

		return null;
	}
	
	public List<AttendanceResponseDto> getAllAttendance(){
		List<Attendance> list = this.attendanceRepo.findAll();
		List<AttendanceResponseDto> attendanceResponseDtoList = new ArrayList<>();
		
		for(Attendance i : list) {
			attendanceResponseDtoList.add(this.attendanceMapper.toAttendanceResponseDto(i));
		}
		
		return attendanceResponseDtoList;
	}
	
	public AttendanceResponseDto getAttendanceById(Integer id){
		Optional<Attendance> attOptional = this.attendanceRepo.findByAttendanceId(id);
		Attendance attendance = attOptional.get();
		return this.attendanceMapper.toAttendanceResponseDto(attendance);
	}
	
	public List<AttendanceResponseDto> getAttendaceByEmpId(Integer id, LocalDateTime start, LocalDateTime end){
		List<Attendance> list = this.attendanceRepo.findAttendanceByEmpId(id,start,end);
		List<AttendanceResponseDto> attendanceResponseDtoList = new ArrayList<>();
		
		for(Attendance i : list) {
			attendanceResponseDtoList.add(this.attendanceMapper.toAttendanceResponseDto(i));
		}
		
		return attendanceResponseDtoList;
	}
	
	
	
	public int deleteAttendance(Integer id) {
		return this.attendanceRepo.deleteByAttendanceId(id);
	}
}
