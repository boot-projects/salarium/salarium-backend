package com.salarium.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.salarium.dto.AddEmployeeDto;
import com.salarium.dto.AdvanceResponseDto;
import com.salarium.dto.EmployeeResponseDto;
import com.salarium.dto.UpdateEmployeeDto;
import com.salarium.entities.Advance;
import com.salarium.entities.Employee;
import com.salarium.mapper.EmployeeMapper;
import com.salarium.repositories.EmployeeRepo;


@Service
public class EmployeeService {
	private final EmployeeRepo employeeRepo;
	private final EmployeeMapper employeeMapper;
	
	public EmployeeService(EmployeeRepo employeeRepo, EmployeeMapper employeeMapper) {
		this.employeeRepo = employeeRepo;
		this.employeeMapper = employeeMapper;
	}
	
	
	public EmployeeResponseDto addEmployee(AddEmployeeDto addEmployeeDto) {
		Employee newEmployee = this.employeeMapper.toEmployee(addEmployeeDto);
		newEmployee.setEmployeeCode(UUID.randomUUID().toString());
		newEmployee = this.employeeRepo.save(newEmployee);
		
		return this.employeeMapper.toEmployeeResponseDto(newEmployee);
	}
	
	public EmployeeResponseDto updateEmployee(Integer existingEmpId,UpdateEmployeeDto updateEmployeeDto) {
		var existingEmployee = this.employeeRepo.findByEmpId(existingEmpId);

		if (existingEmployee != null) {
			Employee updatedEmployee = this.employeeMapper.toEmployee(existingEmpId, updateEmployeeDto);
			updatedEmployee = this.employeeRepo.save(updatedEmployee);

			return this.employeeMapper.toEmployeeResponseDto(updatedEmployee);
		}

		return null;
	}
	
	public List<EmployeeResponseDto> getAllEmployees(){
//		return this.employeeRepo.findAll();
		
		List<Employee> list = this.employeeRepo.findAll();
		List<EmployeeResponseDto> employeeResponseDtoList = new ArrayList<>();
//		List<EmployeeResponseDto> map =list.stream().map((emp)->  this.employeeMapper.toEmployeeResponseDto(emp));
		
		for(Employee i : list) {
			employeeResponseDtoList.add(this.employeeMapper.toEmployeeResponseDto(i));
		}
		
		return employeeResponseDtoList;
	}
	
	public EmployeeResponseDto getEmployeeById(Integer id) {
		
		Optional<Employee> emp = this.employeeRepo.findByEmpId(id);
		Employee employee = emp.get();
		return this.employeeMapper.toEmployeeResponseDto(employee);
	}
	
	public int deleteEmployee(int id) {
		return this.employeeRepo.deleteByEmpId(id);
	}
	
	//get employees with pagination
	public Page<EmployeeResponseDto> getEmployeeswithPagination(int offset, int pageSize) {
		Page<Employee> page = this.employeeRepo.findAll(PageRequest.of(offset, pageSize));
		Page<EmployeeResponseDto> pageDto = page.map((emp) -> this.employeeMapper.toEmployeeResponseDto(emp));
		return pageDto;
	}
	
	
}
