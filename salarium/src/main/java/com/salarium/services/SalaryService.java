package com.salarium.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddSalaryDto;
import com.salarium.dto.SalaryResponseDto;
import com.salarium.dto.UpdateSalaryDto;
import com.salarium.entities.Salary;
import com.salarium.mapper.SalaryMapper;
import com.salarium.repositories.SalaryRepo;

@Service
public class SalaryService {
	
	private final SalaryRepo salaryRepo;
	private final SalaryMapper salaryMapper;
	
	public SalaryService(SalaryRepo salaryRepo, SalaryMapper salaryMapper) {
		this.salaryRepo= salaryRepo;
		this.salaryMapper = salaryMapper;
	}
	
	public SalaryResponseDto addSalary(AddSalaryDto addSalaryDto){
		Salary newSalary = this.salaryMapper.toSalary(addSalaryDto);
		newSalary = this.salaryRepo.save(newSalary);
		return this.salaryMapper.toSalaryResponseDto(newSalary);	
	}
	
	public SalaryResponseDto updateSalary(Integer salaryId, UpdateSalaryDto updateSalaryDto){
		Salary newSalary = this.salaryMapper.toSalary(salaryId, updateSalaryDto);
		newSalary = this.salaryRepo.save(newSalary);
		return this.salaryMapper.toSalaryResponseDto(newSalary);
	}
	
	
	public List<SalaryResponseDto> getAllSalaries(){
		List<Salary> list = this.salaryRepo.findAll();
		List<SalaryResponseDto> dtoList = new ArrayList<>();
		for(Salary sal : list) {
			dtoList.add(this.salaryMapper.toSalaryResponseDto(sal));
		}
		return dtoList;
	}
	
	public SalaryResponseDto getSalaryById(Integer id){
		Salary salary = this.salaryRepo.findBySalaryId(id).get();
		return this.salaryMapper.toSalaryResponseDto(salary);
	}
	
	public List<SalaryResponseDto> getSalaryByEmpId(Integer id, LocalDateTime startDate, LocalDateTime endDate){
		List<Salary> list = this.salaryRepo.findSalaryByEmpId(id,startDate,endDate);
		List<SalaryResponseDto> dtoList = new ArrayList<>();
		for(Salary sal : list) {
			dtoList.add(this.salaryMapper.toSalaryResponseDto(sal));
		}
		return dtoList;
	}
	
	public int deleteSalary(Integer id){
		return this.salaryRepo.deleteBySalaryId(id);
	}
}
