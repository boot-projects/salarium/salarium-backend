package com.salarium.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddLeavesDTO;
import com.salarium.dto.LeavesResponseDto;
import com.salarium.dto.UpdateLeavesDto;
import com.salarium.entities.Leaves;
import com.salarium.mapper.LeavesMapper;
import com.salarium.repositories.LeavesRepo;

@Service
public class LeavesService {
	private final LeavesRepo leavesRepo;

	private final LeavesMapper leavesMapper;

	public LeavesService(LeavesRepo leavesRepo, LeavesMapper leavesMapper) {
		this.leavesRepo = leavesRepo;
		this.leavesMapper = leavesMapper;
	}

	public LeavesResponseDto addLeave(AddLeavesDTO addLeavesDTO) {
		Leaves newLeaves = this.leavesMapper.toLeaves(addLeavesDTO);

		newLeaves = this.leavesRepo.save(newLeaves);

		return this.leavesMapper.toLeavesResponseDto(newLeaves);

	}

	public LeavesResponseDto updateLeaves(Integer leaveId, UpdateLeavesDto updateLeavesDto) {
		var existingLeave = this.leavesRepo.findByLeaveId(leaveId);

		if (existingLeave != null) {
			var updatedLeave = this.leavesMapper.toLeaves(leaveId, updateLeavesDto);

			updatedLeave = this.leavesRepo.save(updatedLeave);

			return this.leavesMapper.toLeavesResponseDto(updatedLeave);
		}

		return null;

	}

	public int deleteLeave(Integer leaveId) {
		return this.leavesRepo.deleteByleaveId(leaveId);
	}

	public List<LeavesResponseDto> getAllLeaves() {
		List<Leaves> list = this.leavesRepo.findAll();
		List<LeavesResponseDto> leavesResponseDtoList = new ArrayList<>();

		for (Leaves i : list) {
			leavesResponseDtoList.add(this.leavesMapper.toLeavesResponseDto(i));
		}

		return leavesResponseDtoList;
	}
	
	public LeavesResponseDto getByLeaveId(Integer leaveId) {
		Leaves leave = this.leavesRepo.findByLeaveId(leaveId);
		
		return this.leavesMapper.toLeavesResponseDto(leave);
	}

	public List<LeavesResponseDto> getAllByEmpId(Integer empId, LocalDateTime startDate, LocalDateTime endDate) {
		List<Leaves> list = this.leavesRepo.findLeavesByEmpId(empId, startDate, endDate);
		List<LeavesResponseDto> leavesResponseDtoList = new ArrayList<>();

		for (Leaves i : list) {
			leavesResponseDtoList.add(this.leavesMapper.toLeavesResponseDto(i));
		}

		return leavesResponseDtoList;
	}

}
