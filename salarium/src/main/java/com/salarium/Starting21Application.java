package com.salarium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Starting21Application {

	public static void main(String[] args) {
		SpringApplication.run(Starting21Application.class, args);
	}
	
	
//	@Bean
//	public CommandLineRunner commandLineRunner(
//			EmployeeRepo employeeRepo) {
//		return args -> {
//			var emp = Employee.builder()
//					.empName("Harshwardhan")
//					.build();
//		};
//	}


}
