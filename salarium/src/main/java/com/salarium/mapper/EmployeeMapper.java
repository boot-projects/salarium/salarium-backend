package com.salarium.mapper;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddEmployeeDto;
import com.salarium.dto.EmployeeResponseDto;
import com.salarium.dto.UpdateEmployeeDto;
import com.salarium.entities.Employee;
@Service
public class EmployeeMapper {
	
	public Employee toEmployee(AddEmployeeDto addEmployeeDto) {
		Employee employee = new Employee();
		employee.setEmpName(addEmployeeDto.getEmpName());
		employee.setEmpContactNo(addEmployeeDto.getEmpContactNo());
		employee.setEmpEmail(addEmployeeDto.getEmpEmail());
		employee.setEmpAltContactNo(addEmployeeDto.getEmpAltContactNo());
		employee.setAddressLine1(addEmployeeDto.getAddressLine1());
		employee.setAddressLine2(addEmployeeDto.getAddressLine2());
		employee.setPincode(addEmployeeDto.getPincode());
		employee.setCity(addEmployeeDto.getCity());
		employee.setState(addEmployeeDto.getState());
		employee.setBankName(addEmployeeDto.getBankName());
		employee.setIfsc(addEmployeeDto.getIfsc());
		employee.setAccountNo(addEmployeeDto.getAccountNo());
		employee.setBankBranch(addEmployeeDto.getBankBranch());
		employee.setSalary(addEmployeeDto.getSalary());
		return employee;
	}
	//update id
	public Employee toEmployee(Integer existingEmpId, UpdateEmployeeDto updateEmployeeDto) {
		Employee employee = new Employee();
		employee.setEmpName(updateEmployeeDto.getEmpName());
		employee.setEmpContactNo(updateEmployeeDto.getEmpContactNo());
		employee.setEmpAltContactNo(updateEmployeeDto.getEmpAltContactNo());
		employee.setEmpEmail(updateEmployeeDto.getEmpEmail());
		employee.setAddressLine1(updateEmployeeDto.getAddressLine1());
		employee.setAddressLine2(updateEmployeeDto.getAddressLine2());
		employee.setPincode(updateEmployeeDto.getPincode());
		employee.setCity(updateEmployeeDto.getCity());
		employee.setState(updateEmployeeDto.getState());
		employee.setBankName(updateEmployeeDto.getBankName());
		employee.setIfsc(updateEmployeeDto.getIfsc());
		employee.setAccountNo(updateEmployeeDto.getAccountNo());
		employee.setBankBranch(updateEmployeeDto.getBankBranch());
		employee.setSalary(updateEmployeeDto.getSalary());
		return employee;
	}

	public EmployeeResponseDto toEmployeeResponseDto(Employee employee) {
		EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
		employeeResponseDto.setEmpId(employee.getEmpId());
		employeeResponseDto.setEmpName(employee.getEmpName());
		employeeResponseDto.setEmpContactNo(employee.getEmpContactNo());
		employeeResponseDto.setEmpAltContactNo(employee.getEmpAltContactNo());
		employeeResponseDto.setAddressLine1(employee.getAddressLine1());
		employeeResponseDto.setAddressLine2(employee.getAddressLine2());
		employeeResponseDto.setPincode(employee.getPincode());
		employeeResponseDto.setCity(employee.getCity());
		employeeResponseDto.setState(employee.getState());
		employeeResponseDto.setBankName(employee.getBankName());
		employeeResponseDto.setIfsc(employee.getIfsc());
		employeeResponseDto.setAccountNo(employee.getAccountNo());
		employeeResponseDto.setBankBranch(employee.getBankBranch());
		employeeResponseDto.setSalary(employee.getSalary());
		employeeResponseDto.setEmpEmail(employee.getEmpEmail());
		
		return employeeResponseDto;
	}
	
}
