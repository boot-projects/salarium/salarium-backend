package com.salarium.mapper;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddLeavesDTO;
import com.salarium.dto.LeavesResponseDto;
import com.salarium.dto.UpdateLeavesDto;
import com.salarium.entities.Employee;
import com.salarium.entities.Leaves;
import com.salarium.repositories.EmployeeRepo;
import com.salarium.services.EmployeeService;

@Service
public class LeavesMapper {
	
	private final EmployeeRepo employeeRepo;
	
	public LeavesMapper(EmployeeRepo employeeRepo) {
		this.employeeRepo = employeeRepo;
	}
	
	
	public Leaves toLeaves(AddLeavesDTO addLeavesDTO) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(addLeavesDTO.getEmpId());
		Employee employee = employeeOptional.get();
		Leaves newLeaves = new Leaves();
		newLeaves.setEmployee(employee);
		newLeaves.setLeaveDate(addLeavesDTO.getLeaveDate());
		newLeaves.setLeaveReason(addLeavesDTO.getLeaveReason());
		newLeaves.setNoOfFullDayLeaves(addLeavesDTO.getNoOfFullDayLeaves());
		newLeaves.setNoOfHalfDayLeaves(addLeavesDTO.getNoOfFullDayLeaves());
		return newLeaves;
	}
	
	public Leaves toLeaves(Integer existingLeaveId, UpdateLeavesDto updateLeavesDto) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(updateLeavesDto.getEmpId());
		Employee employee = employeeOptional.get();
		Leaves updateLeaves = new Leaves();
		updateLeaves.setLeaveId(existingLeaveId);
		updateLeaves.setEmployee(employee);
		updateLeaves.setLeaveDate(updateLeavesDto.getLeaveDate());
		updateLeaves.setLeaveReason(updateLeavesDto.getLeaveReason());
		updateLeaves.setNoOfFullDayLeaves(updateLeavesDto.getNoOfFullDayLeaves());
		updateLeaves.setNoOfHalfDayLeaves(updateLeavesDto.getNoOfFullDayLeaves());
		return updateLeaves;
	}
	
	
	public LeavesResponseDto toLeavesResponseDto(Leaves newLeaves) {
		LeavesResponseDto leavesResponseDto = new LeavesResponseDto();
		leavesResponseDto.setEmpId(newLeaves.getEmployee().getEmpId());
		leavesResponseDto.setLeaveDate(newLeaves.getLeaveDate());
		leavesResponseDto.setLeaveId(newLeaves.getLeaveId());
		leavesResponseDto.setLeaveReason(newLeaves.getLeaveReason());
		leavesResponseDto.setNoOfFullDayLeaves(newLeaves.getNoOfFullDayLeaves());
		leavesResponseDto.setNoOfHalfDayLeaves(newLeaves.getNoOfHalfDayLeaves());
		return leavesResponseDto;
	}
}
