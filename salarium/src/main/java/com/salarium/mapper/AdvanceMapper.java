package com.salarium.mapper;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddAdvanceDto;
import com.salarium.dto.AdvanceResponseDto;
import com.salarium.dto.UpdateAdvanceDto;
import com.salarium.entities.Advance;
import com.salarium.entities.Employee;
import com.salarium.repositories.EmployeeRepo;
import com.salarium.services.EmployeeService;

@Service
public class AdvanceMapper {
	private final EmployeeRepo employeeRepo;
	
	public AdvanceMapper(EmployeeRepo employeeRepo) {
		this.employeeRepo = employeeRepo;
	}
	
	public Advance toAdvance(AddAdvanceDto addAdvanceDto) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(addAdvanceDto.getEmpId());
		Employee employee = employeeOptional.get();
		Advance advance = new Advance();
		advance.setEmployee(employee);
		advance.setAdvanceDate(addAdvanceDto.getAdvanceDate());
		advance.setAdvanceAmount(addAdvanceDto.getAdvanceAmount());
		advance.setReason(addAdvanceDto.getReason());
		return advance;
	}
	
	public Advance toAdvance(Integer existingAdvanceId, UpdateAdvanceDto updateAdvanceDto) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(updateAdvanceDto.getEmpId());
		Employee employee = employeeOptional.get();
		Advance advance = new Advance();
		advance.setAdvanceId(existingAdvanceId);
		advance.setEmployee(employee);
		advance.setAdvanceDate(updateAdvanceDto.getAdvanceDate());
		advance.setAdvanceAmount(updateAdvanceDto.getAdvanceAmount());
		advance.setReason(updateAdvanceDto.getReason());
		return advance;
	}
	
	public AdvanceResponseDto toAdvanceResponseDto(Advance advance) {
		AdvanceResponseDto advanceResponseDto = new AdvanceResponseDto();
		advanceResponseDto.setAdvanceId(advance.getAdvanceId());
		advanceResponseDto.setAdvanceAmount(advance.getAdvanceAmount());
		advanceResponseDto.setAdvanceDate(advance.getAdvanceDate());
		advanceResponseDto.setReason(advance.getReason());
		advanceResponseDto.setEmpId(advance.getEmployee().getEmpId());
		return advanceResponseDto;
	}
	
	
}
