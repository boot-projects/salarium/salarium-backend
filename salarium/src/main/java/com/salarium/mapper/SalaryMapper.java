package com.salarium.mapper;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.salarium.dto.AddSalaryDto;
import com.salarium.dto.SalaryResponseDto;
import com.salarium.dto.UpdateSalaryDto;
import com.salarium.entities.Salary;
import com.salarium.repositories.EmployeeRepo;

@Service
public class SalaryMapper {
	private final EmployeeRepo employeeRepo;
	
	public SalaryMapper(EmployeeRepo employeeRepo) {
		this.employeeRepo = employeeRepo;
	}
	
	public Salary toSalary(AddSalaryDto addSalaryDto) {
		var emp = this.employeeRepo.findByEmpId(addSalaryDto.getEmpId()).orElseThrow(() -> new UsernameNotFoundException("Employee doesn't exist with Employeeid: " + addSalaryDto.getEmpId()));
		Salary salary = new Salary();
//		salary.setsalaryAmount(addSalaryDto.getSalaryAmount());
		salary.setSalaryEmployee(emp);
		salary.setSalaryDate(addSalaryDto.getSalaryDate());
		salary.setsalaryAmount(addSalaryDto.getSalaryAmount());
		salary.settotalAdvance(addSalaryDto.getSalaryAmount());
		salary.setpresentDays(1D);
		salary.setTotalLeaves(1D);
		return salary;
	}

	public Salary toSalary(Integer salaryId, UpdateSalaryDto updateSalaryDto) {
		var emp = this.employeeRepo.findByEmpId(updateSalaryDto.getEmpId()).orElseThrow(() -> new UsernameNotFoundException("Employee doesn't exist with Employeeid: " + updateSalaryDto.getEmpId()));
		Salary salary = new Salary();
		salary.setId(salaryId);
//		salary.setsalaryAmount(updateSalaryDto.getSalaryAmount());
		salary.setSalaryEmployee(emp);
		salary.setSalaryDate(updateSalaryDto.getSalaryDate());
		salary.setsalaryAmount(updateSalaryDto.getSalaryAmount());
		salary.settotalAdvance(updateSalaryDto.getSalaryAmount());
		salary.setpresentDays(1D);
		salary.setTotalLeaves(1D);
//		salary.setSalaryDate(updateSalaryDto.getSalaryDate());
//		salary.setSalaryEmployee(emp);
		return salary;
	}
	
	public SalaryResponseDto toSalaryResponseDto(Salary salary) {
		SalaryResponseDto salaryResponseDto = new SalaryResponseDto();
		salaryResponseDto.setSalaryId(salary.getId());
		salaryResponseDto.setPresentDays(salary.getpresentDays());
		salaryResponseDto.setSalaryAmount(salary.getsalaryAmount());
		salaryResponseDto.setTotalAdvance(salary.gettotalAdvance());
		salaryResponseDto.setSalaryDate(salary.getSalaryDate());
		salaryResponseDto.setEmpId(salary.getSalaryEmployee().getEmpId());
		return salaryResponseDto;
	}
}
