package com.salarium.mapper;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.salarium.dto.AddAttendanceDto;
import com.salarium.dto.AttendanceResponseDto;
import com.salarium.dto.UpdateAttendanceDto;
import com.salarium.entities.Attendance;
import com.salarium.entities.Employee;
import com.salarium.repositories.EmployeeRepo;
import com.salarium.services.EmployeeService;

@Service
public class AttendanceMapper {
	private final EmployeeRepo employeeRepo; 
	
	public AttendanceMapper(EmployeeRepo employeeRepo) {
		this.employeeRepo = employeeRepo;
	}
	
	public Attendance toAttendance(AddAttendanceDto addAttendanceDto) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(addAttendanceDto.getEmpId());
		Employee employee = employeeOptional.get();
		Attendance attendance = new Attendance();
		attendance.setEmployee(employee);
		attendance.setAttendanceDate(addAttendanceDto.getAttendanceDate());
		attendance.setInTime(addAttendanceDto.getInTime());
		attendance.setOutTime(addAttendanceDto.getOutTime());
		attendance.setFullDay(addAttendanceDto.getInTime(), addAttendanceDto.getOutTime());
		attendance.setHalfDay(addAttendanceDto.getInTime(), addAttendanceDto.getOutTime());
		return attendance;
	}
	
	public Attendance toAttendance(Integer attendanceId, UpdateAttendanceDto updateAttendanceDto) {
		Optional<Employee> employeeOptional = this.employeeRepo.findByEmpId(updateAttendanceDto.getEmpId());
		Employee employee = employeeOptional.get();
		Attendance attendance = new Attendance();
		attendance.setEmployee(employee);
		attendance.setAttendanceDate(updateAttendanceDto.getAttendanceDate());
		attendance.setInTime(updateAttendanceDto.getInTime());
		attendance.setOutTime(updateAttendanceDto.getOutTime());
		attendance.setFullDay(updateAttendanceDto.getInTime(), updateAttendanceDto.getOutTime());
		attendance.setHalfDay(updateAttendanceDto.getInTime(), updateAttendanceDto.getOutTime());
		return attendance;
	}
	
	
	
	public AttendanceResponseDto toAttendanceResponseDto(Attendance attendance) {
		AttendanceResponseDto attendanceResponseDto = new AttendanceResponseDto();
		attendanceResponseDto.setAttendanceId(attendance.getAttendanceId());
		attendanceResponseDto.setEmpId(attendance.getEmployee().getEmpId());
		attendanceResponseDto.setAttendanceDate(attendance.getAttendanceDate());
		attendanceResponseDto.setInTime(attendance.getInTime());
		attendanceResponseDto.setOutTime(attendance.getOutTime());
		attendanceResponseDto.setFullDay(attendance.getIsFullDay());
		attendanceResponseDto.setHalfDay(attendance.getIsHalfDay());
		return attendanceResponseDto;
	}
}
